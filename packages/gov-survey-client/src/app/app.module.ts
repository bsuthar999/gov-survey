import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedUIModule } from './shared-ui/shared-ui.module';
import { AppService } from './app.service';
import { SurveyModule } from './survey/survey.module';
import { DynamicFormModule } from './common/dynamic-form/dynamic-form.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OAuthModule.forRoot(),
    DynamicFormModule,
    SharedUIModule,
    SurveyModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent],
})
export class AppModule {}
