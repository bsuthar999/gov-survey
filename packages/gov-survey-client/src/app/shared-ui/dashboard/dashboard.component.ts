import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { IDTokenClaims } from '../../interfaces/id-token-claims.interfaces';
import { ADMINISTRATOR } from '../../constants/roles';
import { CLOSE, LOGIN_AS_ADMINISTRATOR } from '../../constants/messages';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { USER_STATE } from '../../constants/common';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  isLinear = true;
  userState:any = USER_STATE;
  OTP= false;
  progressing=false;
  verificationState = {
    not_initiated:'not_initiated',
    phone_initiated:'phone_initiated',
    phone_verified:'phone_verified',
    code_verified:'code_verified'
  }
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  message:string;
  constructor(
    private _formBuilder: FormBuilder,
    private readonly snackBar: MatSnackBar,
    private oauthService: OAuthService
  ) {}

  ngOnInit() {
    this.userState.status = this.verificationState.not_initiated;
    const idClaims: IDTokenClaims = this.oauthService.getIdentityClaims() || {
      roles: [],
    };
    if (!idClaims.roles.includes(ADMINISTRATOR)) {
      this.message = LOGIN_AS_ADMINISTRATOR;
    }
    this.firstFormGroup = this._formBuilder.group({
      phone: ['', Validators.compose([Validators.required])],
      OTP: ['', Validators.required],
    });
    this.secondFormGroup = this._formBuilder.group({
      card_id: ['', Validators.required],
      card_code: ['', Validators.required],
    });
  }

  sendOtp(){
    this.OTP=false;
    this.userState.status = this.verificationState.phone_initiated;
    this.firstFormGroup.controls.phone.disable();
    this.snackBar.open(`OTP has been sent to ${this.firstFormGroup.controls.phone.value}`,CLOSE,{duration: 3500});
    setTimeout(() => {
      this.OTP = true;
    }, 2500);
  }

  verify(stepper: MatStepper){
    this.progressing= true;
    setTimeout(() => {
      this.progressing= false;
      this.snackBar.open(`Phone Number validated.`,CLOSE,{duration: 3500});
      stepper.next()
    }, 2000);
  }

  final(){
    this.progressing= true;
    setTimeout(() => {
      this.progressing= false;
      this.userState.validated = true;
      this.snackBar.open(`User Authenticated.`,CLOSE,{duration: 3500});
    }, 2000);
  }
}

