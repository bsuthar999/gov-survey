import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { DynamicFormComponent } from '../../common/dynamic-form/containers/dynamic-form/dynamic-form.component';
import { FieldConfig } from '../../common/dynamic-form/models/field-config.interface';

@Component({
  selector: 'app-single-survey',
  templateUrl: './single-survey.component.html',
  styleUrls: ['./single-survey.component.css']
})
export class SingleSurveyComponent implements OnInit {
  @ViewChild(DynamicFormComponent) form: DynamicFormComponent;

  config: { content: FieldConfig[] | any; footer: FieldConfig[] } = {
    content: [
      {
        type: 'input',
        label: 'Full name',
        name: 'name',
        placeholder: 'Enter your name',
        validation: [],
      },
      [
        {
          type: 'select',
          label: 'Average monthly billing',
          name: 'Average monthly billing',
          options: ['less than 1000 INR', '1000- 5000 INR', 'more than 5000 INR'],
          placeholder: 'Select an option',
          validation: [Validators.required],
          width: 50,
        },
        {
          type: 'select',
          label: 'Average percentage of online payment',
          name: 'Average percentage of online payment',
          options: ['0% - 10%', '10% - 30%', '30% - 60%','60% - 90%', '90% - 100%'],
          placeholder: 'Select an option',
          validation: [Validators.required],
          width: 50,
        },
      ],
      [
        {
          type: 'input',
          label: 'Monthly Transaction',
          name: 'transaction',
          placeholder: 'No of transactions per month',
          validation: [Validators.required],
          width: 50,
        },
        {
          type: 'select',
          label: 'Mode of transaction',
          name: 'mode',
          options: ['Google Pay','Bheem UPI','Paytm', 'others'],
          placeholder: 'Select an option',
          validation: [Validators.required],
          width: 50,
        },
      ],
      {
        type: 'input',
        label: 'Most sellable item.',
        name: 'sellable_item',
        placeholder: 'Most sellable Item',
        validation: [Validators.required],
        width: 50,
      },
    ],
    footer: [
      {
        label: 'Submit',
        name: 'submit',
        type: 'button',
        color: 'primary',
      },
      {
        label: 'Danger',
        name: 'Danger',
        type: 'button',
        click: this.click,
        color: 'warn',
      },
    ],
  };

  state: any = {
    title: 'Dummy Survey',
    name: 'Survey Dummy',
  };

  constructor() {}

  ngOnInit(): void {}

  click() {
    // do something
  }

  submit($event?) {}
}
