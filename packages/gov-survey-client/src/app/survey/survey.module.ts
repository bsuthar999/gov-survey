import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../shared-imports/material/material.module';
import { SingleSurveyComponent } from './single-survey/single-survey.component';
import { DynamicFormModule } from '../common/dynamic-form/dynamic-form.module';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MaterialModule,
        ReactiveFormsModule,
        DynamicFormModule
    ],
    declarations: [
        SingleSurveyComponent
    ],
})
export class SurveyModule {}
