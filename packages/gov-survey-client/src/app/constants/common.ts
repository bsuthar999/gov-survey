export const NEW_ID: string = 'new';
export const MODELS = [
  { route: 'client' },
  { route: 'role' },
  { route: 'user' },
  { route: 'scope' },
];
export const AUTHORIZATION = 'Authorization';
export const DURATION = 5000;
export const UNDO_DURATION = 10000;
export const SCOPE = 'openid email roles';
export const USER_STATE = {
  
}